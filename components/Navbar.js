import React from 'react'
import { View, Text } from 'react-native'
import { LinearGradient } from 'expo';
import styles from "../assets/styles/NavbarStyles"
import colors from '../constants/Colors'
import { Constants, Svg } from 'expo';



const Navbar = (title) => {
    const backButton = ()=>{

    }
    return (
        <LinearGradient
        colors={[colors.lightGreen, colors.darkGreen]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        style={styles.containerLinea}>
        <View style={ styles.containerNavbar}>
            <View style={styles.leftContainer}  >
                
             <Svg height="20" width="20"  viewBox="0 0 50 50" space="preserve" >
                    <Svg.Path
                     stroke="#eee"
                     fill="#eee"
                     d="M 0 9 L 0 11 L 50 11 L 50 9 Z M 0 24 L 0 26 L 50 26 L 50 24 Z M 0 39 L 0 41 L 50 41 L 50 39 Z
                     "                       
                        />
                </Svg>
            </View>
            <View style={styles.titleWrapper}  >
                <Text style={styles.text}>{title.title}</Text>
            </View>
            <View style={styles.leftContainer} >
            
            </View>
      </View>
      </LinearGradient>
    )
}

export default Navbar
