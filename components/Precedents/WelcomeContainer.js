import React from 'react'
import { View, Text, Image, } from 'react-native'
import SeleccionarImagen from '../SeleccionarImagen'
import { Constants, Svg } from 'expo';


const WelcomeContainer = ({ style }) => {

    return (
        <View {...style} >
            <View style={{
                flex: 1,
                flexDirection: 'row',

            }
            }>
                <View style={{ width: '55%' }}>

                </View>

                <View style={{
                    width: '45%',

                    flex: 1,



                }}>

                    <SeleccionarImagen style={{
                        //position: 'absolute',
                        width: '100%',
                        height: 20,
                        backgroundColor: 'red',
                        alignItems: 'center',
                        overflow: "hidden",
                        left: 0,
                        right: 0,
                        top: 0,
                    }} />
                    <View style={{

                        position: 'absolute',
                        width: '100 %',
                        height: 20,
                        left: 0,
                        right: 0,
                        bottom: 10,
                        alignItems: 'center',


                    }}>
                        <View style={{ width: 10, height: 10, padding: 12, borderRadius: 15, backgroundColor: '#19769F' }}>

                        </View>
                    </View>
                </View>
            </View>


        </View >

    )
}

/*
box1: {
    position: 'absolute',
        width: '100%',
            height: 20,
                backgroundColor: 'red',
                    alignItems: 'center',
                        overflow: "hidden",
                            left: 0,
                                right: 0,
},
box2: {
    position: 'absolute',
        width: 50,
            height: 20,
                backgroundColor: 'blue',
                    overflow: "hidden",
                        left: 0,
                            right: 0,
},

*/

export default WelcomeContainer
