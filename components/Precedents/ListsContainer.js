import React, { Component } from 'react'

import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { Constants, Svg } from 'expo';
import SeleccionarImagen from '../SeleccionarImagen'
export class ListsContainer extends Component {
    render() {
        return (
            <View>
                <Text>Vacio</Text>
                <Svg height="20" width="20"  viewBox="0 0 50 50" space="preserve" >
                    <Svg.Path
                     stroke="#eee"
                     fill="#eee"
                     d="M 0 9 L 0 11 L 50 11 L 50 9 Z M 0 24 L 0 26 L 50 26 L 50 24 Z M 0 39 L 0 41 L 50 41 L 50 39 Z
                     "                       
                        />
                </Svg>
            </View>
        )
    }
}

export default ListsContainer
