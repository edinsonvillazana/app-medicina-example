import React, { Component } from 'react'
import { Text, ScrollView, Image, View } from 'react-native'
import WelcomeContainer from './WelcomeContainer'
import InforContainer from './InfoContainer'
import ListsContainer from './ListsContainer'
import style from '../../assets/styles/PrecedentsContainerStyle'

export class PrecedentsContainer extends Component {
    render() {

        return (
            < ScrollView >
                <WelcomeContainer style={style.welcomeContainer} />
                <InforContainer style={style.infoContainer} />
                <ListsContainer />
            </ScrollView >
        )
    }
}

export default PrecedentsContainer
