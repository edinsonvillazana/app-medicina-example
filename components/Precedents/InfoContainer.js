import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import styleInfo from '../../assets/styles/InfoContainerStyle'
import InfoIcoins from './InfoIcons'
import { Constants, Svg } from 'expo';


const InfoContainer = ({ style }) => {

    return (
        <View {...style}>
            <View style={styleInfo.leftContainer}>
                <View style={{
                    flex: 1,
                    flexDirection: 'column',

                }
                }>
                    <View style={{ width: '100%', }}><Text style={{ textAlign: 'left' }}>Tipo de sangre</Text></View>
                    <View style={{ width: '100%', }}><Text style={{ fontSize: 14.5, color: '#19789f', fontWeight: 'bold' }}>Tipo de sangre</Text></View>
                </View>
            </View>
            <View style={{

                borderWidth: 0.5,
                borderColor: '#78b4c5',
                borderBottomWidth: 0,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 5 },
                shadowOpacity: 0.9,

                elevation: 1,
                height: '80%'

            }} />
            <View style={styleInfo.leftContainer}>
                <View style={{
                    flex: 1,
                    flexDirection: 'column',

                }
                }>
                    <View style={{ width: '100%' }}><Text style={{ textAlign: 'center' }}>Edad</Text></View>
                    <View style={{
                        width: '100%',
                        overflow: 'hidden'
                    }}>

                    </View>
                </View>
            </View>
            <View style={{

                borderWidth: 0.5,
                borderColor: '#78b4c5',
                borderBottomWidth: 0,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 5 },
                shadowOpacity: 0.9,

                elevation: 1,
                height: '80%'

            }} />
            <View style={styleInfo.leftContainer}>
                <InfoIcoins />
            </View>

        </View >

    )
}

const styles2 = StyleSheet.create({
    container: {
        flex: 1
    },
    box1: {
        position: 'absolute',
        //top: 40,
        //left: 40,
        width: '100%',

        backgroundColor: 'red',
        alignItems: 'center'
    },
    box2: {
        position: 'absolute',
        // left: 80,
        width: 50,
        backgroundColor: 'blue'
    },

    text: {
        color: '#ffffff',
        fontSize: 80
    }
});

export default InfoContainer
