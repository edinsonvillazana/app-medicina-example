import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import GenericScreens from "../components/GenericScreens"


import MainTabNavigator from './MainTabNavigator';

export default createAppContainer(createSwitchNavigator({

  Main: MainTabNavigator,
}));