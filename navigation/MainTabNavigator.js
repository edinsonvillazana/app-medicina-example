import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import GenericScreens from "../components/GenericScreens"


import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import PrecedentsScreen from '../screens/PrecedentsScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (

    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const LinksStack = createStackNavigator({
  Links: LinksScreen,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};



const PrecedentsStack = createStackNavigator({
  Precedents: PrecedentsScreen,
});

PrecedentsStack.navigationOptions = {
  tabBarLabel: 'Antecedentes',

};




export default createBottomTabNavigator({
  PrecedentsStack,
  HomeStack,
  LinksStack,

}

);

/*

export default createBottomTabNavigator({
  HomeStack: { screen: GenericScreens },
  LinksStack: { screen: GenericScreens },
  SettingsStack: { screen: GenericScreens },
});*/
