import React from 'react';
import Navbar from '../components/Navbar'
import PrecedentsContainer from '../components/Precedents'

import {

    StyleSheet,

    View,
} from 'react-native';


export default class PrecedentsScreen extends React.Component {
    static navigationOptions = {
        header: null,
        title: 'Precedents',
    };

    render() {
        return (
            <View style={styles.container}>
                <Navbar title={'Agregar Antecedentes Quirúrgicos'} />
                <PrecedentsContainer />
            </View>
        );
    }
}

const styles = StyleSheet.create({

});
