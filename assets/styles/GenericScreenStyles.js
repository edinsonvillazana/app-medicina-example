import { Text, StyleSheet, View } from 'react-native'

export default styles = StyleSheet.create({

    containerLinea: {
        height: 84,
        borderWidth: 0.5,
        borderBottomColor: '#bbb',
        borderBottomWidth: 0.5,

    },

    title: {
        marginTop: 20,
        height: 64,
        alignItems: "center",
        justifyContent: "center",
    },

    text: {
        fontSize: 19,

        color: 'white',
        fontWeight: 'bold'
    }

})
