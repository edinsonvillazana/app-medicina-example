import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({

    leftContainer: {
        width: '33%',
        justifyContent: "center",

        height: '100%',
        paddingVertical: 3,
        paddingHorizontal: 2,
        //backgroundColor: 'blue',
    },

    rightContainer: {
        width: '32%',
        alignItems: "center",
        justifyContent: "center"

    },

    titleWrapper: {
        width: '32%',
        alignItems: "center",
        justifyContent: "center",
        paddingLeft: 10,
        paddingRight: 10,
    },


})

