import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({

    containerLinea: {
        height: 80,
        borderWidth: 0.5,
        borderBottomColor: '#bbb',
        borderBottomWidth: 0.5,


    },
    navBar: {
        height: 80,
        borderWidth: 0.5,
        borderBottomColor: '#bbb',
        borderBottomWidth: 0.5,

    },
    containerNavbar: {
        flexDirection: 'row',
        marginTop: 20,
        height: 64,
        alignItems: "center",
        justifyContent: "center",

    },
    leftContainer: {
        width: 45,
        alignItems: "center",
        justifyContent: "center",

    },

    rightContainer: {
        width: 45,
        alignItems: "center",
        justifyContent: "center"
    },

    titleWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",

        paddingLeft: 10,
        paddingRight: 10,
    },

    text: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'bold',
        textAlign: "center",
    }

})
