import { Text, StyleSheet, View } from 'react-native'

export default styles = StyleSheet.create({

    container: {

        marginBottom: 16,
        //justifyContent: "flex-start",
        //alignItems: "stretch",
        backgroundColor: "#ffffff",
        flex: 1
    },

    welcomeContainer: {
        height: 140,
        flex: 1
    },
    infoContainer: {

        flex: 1,
        alignItems: "center",
        height: 55,
        backgroundColor: "#f7f7f7",
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderTopColor: "#e1e1e1",
        borderBottomColor: "#e1e1e1",
        flexDirection: 'row',
    },

    listsContainer: {

        paddingVertical: 38,
        paddingHorizontal: 20,

    }

})
